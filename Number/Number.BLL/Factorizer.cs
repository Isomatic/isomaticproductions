﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Numbers.Models;
using Numbers.DAL;

namespace Numbers.BLL
{
    public static class Factorizer
    {
        public static Factorized Prime(int num)
        {
            First1000Primes primes = new First1000Primes();
            Factorized factorized = new Factorized();
            List<int> factors = new List<int>();

            for(int i=num; i>0; i--)
            {
                if(num%i == 0)
                {
                    factors.Add(i);
                }
            }

            factorized.Divisors = factors;
            factorized.SumOfDivisors = factors.Sum();
            factorized.IsPrime = false;

            if(factors.Count == 2 && num <= 7919)
            {
                factorized.IsPrime = true;               
                factorized.PrimePosition = primes.TheFirst1000Primes[num];
                factorized.Prime = primes.TheFirst1000Primes.SingleOrDefault(n => n.Value == num).Key;
            }
            else if (factors.Count == 2)
            {
                factorized.Prime = -1;
                factorized.IsPrime = true;
            }
            else
            {          
              factorized.Prime = primes.TheFirst1000Primes.SingleOrDefault(n => n.Value == num).Key;          
            }

            return factorized;
        }
    }
}
