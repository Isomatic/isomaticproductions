﻿using Numbers.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbers.BLL
{
    public static class ValueAssigner
    {       
        public static Word AssignValues(Word word)
        {
            GematriaCalculator calc = new GematriaCalculator();
            word.Jewish = calc.Jewish(word.Name);
            word.English = calc.SimpleEnglish(word.Name) * 6;
            word.SimpleEnglish = calc.SimpleEnglish(word.Name);
            word.EnglishReduction = calc.EnglishReduction(word.Name);
            word.Pythagorean = calc.Pythagorean(word.Name);
            word.SExceptionPythagorean = calc.SException(word.Name);

            return word;
        }
    }
}
