﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Numbers.BLL;
using Numbers.Models;
using Numbers.DAL;

namespace Numbers.BLL
{
    public class StatisticsService
    {
        public List<Gematria> Compile(string input, List<Gematria> gematriaList, Dictionary<int, int> tally)
        {
            if (string.IsNullOrWhiteSpace(input))
            {
                return gematriaList;
            }

            int num = 0;

            if (!int.TryParse(input, out num))
            {
                foreach (var gematria in CalculateGematria(input))
                {
                    if (!IsReflection(gematria.Value, gematriaList) &&
                        !gematriaList.Exists(x => x.Value == gematria.Value))
                    {
                        gematriaList.Add(gematria);

                        if(!tally.ContainsKey(gematria.Value))
                        tally.Add(gematria.Value, 1);
                    }
                    else
                    {
                        if (tally.ContainsKey(gematria.Value))
                        {
                            tally[gematria.Value]++;
                        }
                        else
                        {
                            tally[Reflect(gematria.Value)]++;
                        }
                    }
                }
            }
            else
            {
                if(!gematriaList.Exists(x=>x.Value == num))
                gematriaList.Add(new Gematria {Value = num, Method = GematriaTypes.NULL});
            }

            return gematriaList;
        }

        public List<Gematria> GetGematria()
        {
            var repo = new GematriaStatsRepository();
            return repo.GetGematria();
        }

        public Gematria AddGematria(Gematria gematria)
        {
            var repo = new GematriaStatsRepository();
            return repo.AddGematria(gematria);
        }

        private bool IsReflection(int num, List<Gematria> gematria) // 47 52
        {
            foreach (var val in gematria)
            {
                var num1 = val.Value.ToString().ToCharArray();
                var num2 = num.ToString().Reverse().ToArray();

                if (num1.Length == num2.Length && num1.Intersect(num2).Count() == num1.Length)
                    return true;
            }

            return false;
        }

        private int Reflect(int num)
        {
            var reflect = num.ToString().Reverse().ToArray();
            var result = "";

            foreach (var c in reflect)
            {
                result += c;
            }

            return Convert.ToInt32(result);
        }

        private Gematria[] CalculateGematria(string word)
        {
            var calc = new GematriaCalculator();

            return new Gematria[]
            {
                new Gematria
                {
                    Value = calc.SimpleEnglish(word),
                    Method = GematriaTypes.EnglishOrdinal
                },
                new Gematria
                {
                    Value = calc.EnglishReduction(word),
                    Method = GematriaTypes.FullReduction
                },
                new Gematria
                {
                    Value = calc.Gematrinate(word, GematriaTypes.ReverseOrdinal),
                    Method = GematriaTypes.ReverseOrdinal
                },
                new Gematria
                {
                    Value = calc.Gematrinate(word, GematriaTypes.ReverseReduced),
                    Method = GematriaTypes.ReverseReduced
                }
            };

        }
    }
}
