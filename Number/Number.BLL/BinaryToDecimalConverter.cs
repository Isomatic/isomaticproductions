﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbers.BLL
{
    public static class BinaryToDecimalConverter
    {
        public static int Convert(string binary)
        {
            var result = 0;
            var length = binary.Length-1;

            if(!ContainsBinary(binary))
            return -1;

            int j = 1;
            for (int i = length; i > 0; i--)
            {
                result += binary[i] == '0' ? 0 : 1*j;
                j *= 2;
            }

            return result;
        }

        private static bool ContainsBinary(string binary)
        {
            foreach (var digit in binary)
            {
                if (digit != '0' && digit != '1')
                {
                    return false;
                }
            }

            return true;          
        }
    }
}
