﻿using System;

namespace Numbers.BLL
{
    public class DateRange
    {
        public bool HasDates { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateRange() { }

        public DateRange(bool hasDates, DateTime startDate, DateTime endDate)
        {
            HasDates = hasDates;
            StartDate = startDate;
            EndDate = endDate;
        }
    }
}
