﻿using System;
using System.Collections.Generic;
using Number.BLL.Contracts;

namespace Numbers.BLL
{
    public class DateCalculator : Calculator
    {
        public int LifeLesson(DateTime date)
        {
            var year = date.Year.ToString();
            int numerology = 0;

            foreach (var digit in year)
            {
                int num = 0;
                int.TryParse(digit.ToString(), out num);
                numerology += num;
            }

            return date.Month + date.Day + numerology;
        }

        public int Split(DateTime date)
        {
            var year = date.Year.ToString();
            int numerology = 0;

            for (int i = 0; i <= 2; i = i + 2)
            {
                int num = 0;
                int.TryParse(year.Substring(i, year.Length / 2), out num);
                numerology += num;
            }

            return date.Month + date.Day + numerology;
        }

        public int AbrvSplit(DateTime date)
        {
            var year = date.Year.ToString();
            int numerology = 0;

            for (int i = 2; i <= 2; i = i + 2)
            {
                int num = 0;
                int.TryParse(year.Substring(i, year.Length / 2), out num);
                numerology += num;
            }

            return date.Month + date.Day + numerology;
        }

        public int PythSplit(DateTime date)
        {
            return Reduce(date.Day) + Reduce(date.Month) + Split(date) - date.Month - date.Day;
        }

        public int MonthDay(DateTime date)
        {
            return date.Month + date.Day;
        }

        public int Pythagorean(DateTime date)
        {
            string pieces;
            int numerology = 0;

            pieces = date.Month.ToString() + date.Day + date.Year;

            foreach (var num in pieces)
            {
                int n = 0;
                int.TryParse(num.ToString(), out n);
                numerology += n;
            }
            return numerology;
        }

        public IEnumerable<DateTime> LoopThrough()
        {
            var first = new DateTime(1001, 01, 01);
            var last = new DateTime(2030, 12, 31);
            for (var day = first.Date; day.Date <= last.Date; day = day.AddDays(1))
                yield return day;
        }

        public TimeSpan DateSpan(DateTime startDate, DateTime endDate)
        {
            //int[] days = new int[2];
            //days[0] = (int)(startDate - endDate).TotalDays - 1;
            //days[1] = (int)(startDate - endDate).TotalDays;

            return startDate - endDate;
        }
    }
}
