﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Numbers.BLL.Contracts;
using Numbers.Models;

namespace Numbers.BLL.Translators
{
    class GematriaTranslator : ITranslator
    {
        private readonly Dictionary<char, int> _letterValues = new Dictionary<char, int>();
        private GematriaTypes _gematriaType;

        public GematriaTranslator(GematriaTypes gematriaType)
        {
            _gematriaType = gematriaType;

            switch (gematriaType)
            {
                /*The default Pythagorean calculation uses the '1 S exception' and the
                11 and 22 K and V exceptions respectively */
                case GematriaTypes.PythagoreanKv:
                    _letterValues['A'] = 1;
                    _letterValues['B'] = 2;
                    _letterValues['C'] = 3;
                    _letterValues['D'] = 4;
                    _letterValues['E'] = 5;
                    _letterValues['F'] = 6;
                    _letterValues['G'] = 7;
                    _letterValues['H'] = 8;
                    _letterValues['I'] = 9;
                    _letterValues['J'] = 1;
                    _letterValues['K'] = 11;
                    _letterValues['L'] = 3;
                    _letterValues['M'] = 4;
                    _letterValues['N'] = 5;
                    _letterValues['O'] = 6;
                    _letterValues['P'] = 7;
                    _letterValues['Q'] = 8;
                    _letterValues['R'] = 9;
                    _letterValues['S'] = 1;
                    _letterValues['T'] = 2;
                    _letterValues['U'] = 3;
                    _letterValues['V'] = 22;
                    _letterValues['W'] = 5;
                    _letterValues['X'] = 6;
                    _letterValues['Y'] = 7;
                    _letterValues['Z'] = 8;
                    break;

                case GematriaTypes.Jewish:
                    _letterValues['A'] = 1;
                    _letterValues['B'] = 2;
                    _letterValues['C'] = 3;
                    _letterValues['D'] = 4;
                    _letterValues['E'] = 5;
                    _letterValues['F'] = 6;
                    _letterValues['G'] = 7;
                    _letterValues['H'] = 8;
                    _letterValues['I'] = 9;
                    _letterValues['J'] = 600;
                    _letterValues['K'] = 10;
                    _letterValues['L'] = 20;
                    _letterValues['M'] = 30;
                    _letterValues['N'] = 40;
                    _letterValues['O'] = 50;
                    _letterValues['P'] = 60;
                    _letterValues['Q'] = 70;
                    _letterValues['R'] = 80;
                    _letterValues['S'] = 90;
                    _letterValues['T'] = 100;
                    _letterValues['U'] = 200;
                    _letterValues['V'] = 700;
                    _letterValues['W'] = 900;
                    _letterValues['X'] = 300;
                    _letterValues['Y'] = 400;
                    _letterValues['Z'] = 500;
                    break;

                case GematriaTypes.ALW:
                    _letterValues['A'] = 1;
                    _letterValues['B'] = 20;
                    _letterValues['C'] = 13;
                    _letterValues['D'] = 6;
                    _letterValues['E'] = 25;
                    _letterValues['F'] = 18;
                    _letterValues['G'] = 11;
                    _letterValues['H'] = 4;
                    _letterValues['I'] = 23;
                    _letterValues['J'] = 16;
                    _letterValues['K'] = 9;
                    _letterValues['L'] = 2;
                    _letterValues['M'] = 21;
                    _letterValues['N'] = 14;
                    _letterValues['O'] = 7;
                    _letterValues['P'] = 26;
                    _letterValues['Q'] = 19;
                    _letterValues['R'] = 12;
                    _letterValues['S'] = 5;
                    _letterValues['T'] = 24;
                    _letterValues['U'] = 17;
                    _letterValues['V'] = 10;
                    _letterValues['W'] = 3;
                    _letterValues['X'] = 22;
                    _letterValues['Y'] = 15;
                    _letterValues['Z'] = 8;
                    break;

                case GematriaTypes.ReverseOrdinal:
                    _letterValues['A'] = 26;
                    _letterValues['B'] = 25;
                    _letterValues['C'] = 24;
                    _letterValues['D'] = 23;
                    _letterValues['E'] = 22;
                    _letterValues['F'] = 21;
                    _letterValues['G'] = 20;
                    _letterValues['H'] = 19;
                    _letterValues['I'] = 18;
                    _letterValues['J'] = 17;
                    _letterValues['K'] = 16;
                    _letterValues['L'] = 15;
                    _letterValues['M'] = 14;
                    _letterValues['N'] = 13;
                    _letterValues['O'] = 12;
                    _letterValues['P'] = 11;
                    _letterValues['Q'] = 10;
                    _letterValues['R'] = 9;
                    _letterValues['S'] = 8;
                    _letterValues['T'] = 7;
                    _letterValues['U'] = 6;
                    _letterValues['V'] = 5;
                    _letterValues['W'] = 4;
                    _letterValues['X'] = 3;
                    _letterValues['Y'] = 2;
                    _letterValues['Z'] = 1;
                    break;

                case GematriaTypes.ReverseReduced:
                    _letterValues['A'] = 8;
                    _letterValues['B'] = 7;
                    _letterValues['C'] = 6;
                    _letterValues['D'] = 5;
                    _letterValues['E'] = 4;
                    _letterValues['F'] = 3;
                    _letterValues['G'] = 2;
                    _letterValues['H'] = 1;
                    _letterValues['I'] = 9;
                    _letterValues['J'] = 8;
                    _letterValues['K'] = 7;
                    _letterValues['L'] = 6;
                    _letterValues['M'] = 5;
                    _letterValues['N'] = 4;
                    _letterValues['O'] = 3;
                    _letterValues['P'] = 2;
                    _letterValues['Q'] = 1;
                    _letterValues['R'] = 9;
                    _letterValues['S'] = 8;
                    _letterValues['T'] = 7;
                    _letterValues['U'] = 6;
                    _letterValues['V'] = 5;
                    _letterValues['W'] = 4;
                    _letterValues['X'] = 3;
                    _letterValues['Y'] = 2;
                    _letterValues['Z'] = 1;
                    break;

                //Simple English Gematria
                default:
                    _letterValues['A'] = 1;
                    _letterValues['B'] = 2;
                    _letterValues['C'] = 3;
                    _letterValues['D'] = 4;
                    _letterValues['E'] = 5;
                    _letterValues['F'] = 6;
                    _letterValues['G'] = 7;
                    _letterValues['H'] = 8;
                    _letterValues['I'] = 9;
                    _letterValues['J'] = 10;
                    _letterValues['K'] = 11;
                    _letterValues['L'] = 12;
                    _letterValues['M'] = 13;
                    _letterValues['N'] = 14;
                    _letterValues['O'] = 15;
                    _letterValues['P'] = 16;
                    _letterValues['Q'] = 17;
                    _letterValues['R'] = 18;
                    _letterValues['S'] = 19;
                    _letterValues['T'] = 20;
                    _letterValues['U'] = 21;
                    _letterValues['V'] = 22;
                    _letterValues['W'] = 23;
                    _letterValues['X'] = 24;
                    _letterValues['Y'] = 25;
                    _letterValues['Z'] = 26;
                    break;
            }
        }

        public int Translate(char letter)
        {
            int value;

            if (_letterValues.ContainsKey(char.ToUpper(letter)))
            {
                value = _letterValues[char.ToUpper(letter)];
            }
            else
            {
                value = 0;
            }

            return value;
        }
    }
}
