﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Numbers.DAL;

namespace Numbers.BLL
{
    public static class Sequence
    {
        public static int Sum1ThruNum(int num)
        {
            int sum = 0;

            for (int i = 1; i <= num; i++)
            {
                sum += i;
            }

            return sum;
        }

        public static int Fibonacci(int n)
        {
            if (n > 33)
            {
                return -1;
            }
            if (n == 0)
            {
                return 0;
            }
            else if (n < 2)
            {
                return 1;
            }
            else
            {
                return Fibonacci(n - 1) + Fibonacci(n - 2);
            }
        }

        public static int GetFibonacciOrdinal(int n)
        {
            var fibs = new First33Fibonaccis();

            return fibs.GetFibonacciOrdinal(n);
        }
    }
}

