﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Number.BLL.Contracts;
using Numbers.Models;
using Numbers.BLL.Translators;

namespace Numbers.BLL
{
    public class GematriaCalculator : Calculator
    {
        public int SimpleEnglish(string word)
        {
            GematriaTranslator translator = new GematriaTranslator(GematriaTypes.EnglishOrdinal);
            int value = 0;
            foreach (var letter in word)
            {
                value += translator.Translate(letter);
            }

            return value;
        }

        public int Jewish(string word)
        {
            int value = 0;
            GematriaTranslator translator = new GematriaTranslator(GematriaTypes.Jewish);
            
            foreach(var letter in word)
            {
                value += translator.Translate(letter);
            }

            return value;
        }

        public int Pythagorean(string word)
        {
            GematriaTranslator translator = new GematriaTranslator(GematriaTypes.PythagoreanKv);
            int value = 0;

            foreach (var letter in word)
            {
                value += translator.Translate(letter);
            }

            return value;
        }

        public int Gematrinate(string word, GematriaTypes system, bool reduced = false, bool sException = false, bool kvException = false)
        {
            int value = 0;
            GematriaTranslator translator = new GematriaTranslator(system);

            foreach (var letter in word)
            {
                if (reduced)
                {
                    value += Reduce(translator.Translate(letter));
                }
                else
                {
                    value += translator.Translate(letter);
                }         
            }

            return value;
        }

        public int KvsException(string word)
        {
            int counter = 0;

            foreach (var letter in word)
            {
                if (char.ToUpper(letter) == 'S')
                {
                    counter += 9;
                }
            }

            int value = Pythagorean(word) + counter;
            return value;
        }

        public int SException(string word)
        {
            int counter = 0;

            foreach (var letter in word)
            {
                if (char.ToUpper(letter) == 'S')
                {
                    counter += 9;
                }
            }

            int value = EnglishReduction(word) + counter;

            return value;
        }

        public int EnglishReduction(string word)
        {
            int counter = 0;

            foreach(var letter in word)
            {
                if(char.ToUpper(letter) == 'K')
                {
                    counter += 9;
                }
                else if(char.ToUpper(letter) == 'V')
                {
                    counter += 18;
                }
            }

            int result = Pythagorean(word) - counter;
            return result;
        }

        public int Vowelless(string word)
        {
            GematriaTranslator translator = new GematriaTranslator(GematriaTypes.EnglishOrdinal);
            int value = 0;

            foreach (var letter in word)
            {
                char key = char.ToUpper(letter);

                if (key == 'A' || key == 'E' || key == 'I' || key == 'O' || key == 'U')
                {
                    continue;
                }
                else
                {
                    value += translator.Translate(key);
                }
            }

            return value;
        }

        public int VowellessReduction(string word)
        {
            int value = 0;

            foreach (var letter in word)
            {
                char key = char.ToUpper(letter);

                if (key == 'A' || key == 'E' || key == 'I' || key == 'O' || key == 'U')
                {
                    continue;
                }
                else
                {
                    value += EnglishReduction(word);
                }
            }

            return value;
        }
    }
}
