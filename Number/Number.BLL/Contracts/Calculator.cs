﻿namespace Number.BLL.Contracts
{
    public class Calculator
    {
        public int Reduce(int num)
        {
            var wr = num.ToString();
            int sum = 0;  
          
            foreach (var n in wr)
            {
                int temp;
                int.TryParse(n.ToString(), out temp);
                sum += temp;
            }

            return sum;
        }
    }
}
