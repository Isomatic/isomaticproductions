﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbers.BLL.Contracts
{
    public interface ITranslator
    {
        int Translate(char letter);
    }
}
