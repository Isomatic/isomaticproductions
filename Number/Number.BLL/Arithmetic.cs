﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbers.BLL
{
    public static class Arithmetic
    {
        public static int Add(string word)
        {
            int sum = 0;
            int num = 0;
            string[] numbers = new string[20];
            numbers = word.Split('+');

            foreach (var number in numbers)
            {
                int.TryParse(number, out num);
                sum += num;
            }

            return sum;
        }

        public static int Subtract(string word, int temp)
        {
            string[] numbers = word.Split('-');
            int num = 0;

            foreach (var n in numbers)
            {
                int.TryParse(n, out num);
                temp -= num;
            }

            return temp;
        }

        public static DateTime Dates(string word)
        {
            //DateTime T
            //DateTime.TryParse(word, out)
            throw new NotImplementedException();
        }
    }
}
