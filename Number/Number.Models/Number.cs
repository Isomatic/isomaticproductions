﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbers.Models
{
    public class Number
    {
        public int Value { get; set; }
        public List<Word> Words { get; set; }
    }
}
