﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbers.Models
{
    public class Factorized
    {
        public bool IsPrime { get; set; }
        public int? PrimePosition { get; set; }
        public int? Prime { get; set; }
        public int SumOfDivisors { get; set; }
        public List<int> Divisors { get; set; }

        public Factorized()
        {
            Divisors = new List<int>();
        }
    }
}
