﻿namespace Numbers.Models
{
    public class Word
    {
        public string Name { get; set; }
        public GematriaTypes GematriaType { get; set; }
        public bool SException { get; set; }
        public bool KVexception { get; set; }
        public int Jewish { get; set; }
        public int SimpleEnglish { get; set; }
        public int English { get; set; }
        public int EnglishReduction { get; set; }
        public int Pythagorean { get; set; }
        public int SExceptionPythagorean { get; set; }
        public int Vowelless { get; set; }
        public int VowellessReduction { get; set; }
    }
}