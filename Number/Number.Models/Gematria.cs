﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Numbers.Models;

namespace Numbers.Models
{
    public class Gematria
    {
        public int Value { get; set; }
        public GematriaTypes Method { get; set; }
    }
}
