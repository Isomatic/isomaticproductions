﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Numbers.Models;

namespace Numbers.Models.Contracts
{
    public interface INumberRepository
    {
        List<Number> GetAll();
        Number Get(int number);
        void Add(Number number);
    }
}
