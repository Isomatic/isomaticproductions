﻿namespace Numbers.Models
{
    public enum GematriaTypes
    {
        EnglishOrdinal,
        FullReduction,
        English,
        PythagoreanKv,
        Jewish,
        ALW,
        ReverseOrdinal,
        ReverseReduced,
        NULL
    }
}