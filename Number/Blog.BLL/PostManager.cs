﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Blog.Models;
using Blog.DAL;
using Blog.Models.Contracts;

namespace Blog.BLL
{
    public class PostManager
    {
        private IPostRepository _posts = new PostRepository();

        public PostManager(IPostRepository repo)
        {
            _posts = repo;
        }
        public List<Post> GetAll()
        {
            return _posts.GetPosts();
        }

        public Post Get(int id)
        {
            return _posts.GetPost(id);
        }

        public void Add(Post post)
        {
            if(_posts.GetPosts().Count == 0)
            {
                post.Id = 1;
            }
            else
            {
                post.Id = _posts.GetPosts().Max(i => i.Id) + 1;
            }

            _posts.Add(post);
        }
    }
}
