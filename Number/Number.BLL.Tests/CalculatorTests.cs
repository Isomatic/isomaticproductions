﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Numbers.Models;

namespace Numbers.BLL.Tests
{
    [TestFixture]
    public class CalculatorTests
    {
        [TestCase("Propaganda", 93)]
        [TestCase("Freemason", 96)]
        [TestCase("Mason", 62)]
        [TestCase("Title", 66)]
        [TestCase("Masonic", 74)]
        [TestCase("Slave", 59)]
        public void SimpleEnglishTest(string testWord, int expected)
        {
            string word = testWord;
            GematriaCalculator calc = new GematriaCalculator();
            var actual = calc.Gematrinate(word, GematriaTypes.EnglishOrdinal);

            Assert.AreEqual(expected, actual);
        }

        [TestCase("Banker", 33)]
        [TestCase("Michael", 33)]
        [TestCase("Mason", 17)]
        [TestCase("Freemasonry", 58)]
        [TestCase("Prophecy", 52)]
        [TestCase("Hanks", 26)]
        [TestCase("Shanks", 27)]
        public void PythagoreanTest(string testWord, int expected)
        {
            string word = testWord;
            GematriaCalculator calc = new GematriaCalculator();
            var actual = calc.SException(word);

            Assert.AreEqual(expected, actual);
        }

        [TestCase("Hanks", 35)]
        [TestCase("Shanks", 45)]
        public void Pythagorean10SExceptionTest(string testWord, int expected)
        {
            string word = testWord;
            GematriaCalculator calc = new GematriaCalculator();
            var actual = calc.KvsException(word);

            Assert.AreEqual(expected, actual);
        }

        [TestCase("Banker", 24)]
        [TestCase("Mason", 17)]
        [TestCase("Michael", 33)]
        [TestCase("February", 42)]
        public void EnglishReductionTest(string testWord, int expected)
        {
            GematriaCalculator calc = new GematriaCalculator();
            string word = testWord;
            var actual = calc.EnglishReduction(word);

            Assert.AreEqual(expected, actual);           
        }

        [TestCase("Obama", 84)]
        [TestCase("Mason", 211)]
        [TestCase("America", 129)]
        [TestCase("Beatles", 223)]
        public void JewishTest(string testWord, int expected)
        {
            GematriaCalculator calc = new GematriaCalculator();
            string word = testWord;
            var actual = calc.Jewish(word);

            Assert.AreEqual(expected, actual);
        }

        [TestCase("0", 0)]
        [TestCase("00000000", 0)]
        [TestCase("10011", 19)]
        [TestCase("0010", 2)]
        [TestCase("11011000", 216)]
        [TestCase("0000001010011010", 666)]
        public void BinaryConverterTest(string binary, int expected)
        {
           var actual = BinaryToDecimalConverter.Convert(binary);

           Assert.AreEqual(expected, actual);
        }

    }
}
