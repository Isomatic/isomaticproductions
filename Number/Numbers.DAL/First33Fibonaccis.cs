﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbers.DAL
{
    public class First33Fibonaccis
    {
        private readonly Dictionary<int, int> _fibonacci;

        public First33Fibonaccis()
        {
            _fibonacci = new Dictionary<int, int>();
            _fibonacci[1] = 1;
            _fibonacci[2] = 1;
            _fibonacci[3] = 2;
            _fibonacci[4] = 3;
            _fibonacci[5] = 5;
            _fibonacci[6] = 8;
            _fibonacci[7] = 13;
            _fibonacci[8] = 21;
            _fibonacci[9] = 34;
            _fibonacci[10] = 55;
            _fibonacci[11] = 89;
            _fibonacci[12] = 144;
            _fibonacci[13] = 233;
            _fibonacci[14] = 377;
            _fibonacci[15] = 610;
            _fibonacci[16] = 987;
            _fibonacci[17] = 1597;
            _fibonacci[18] = 2584;
            _fibonacci[19] = 4181;
            _fibonacci[20] = 6765;
            _fibonacci[21] = 10946;
            _fibonacci[22] = 17711;
            _fibonacci[23] = 28657;
            _fibonacci[24] = 46368;
            _fibonacci[25] = 75025;
            _fibonacci[26] = 121393;
            _fibonacci[27] = 196418;
            _fibonacci[28] = 317811;
            _fibonacci[29] = 514229;
            _fibonacci[30] = 832040;
            _fibonacci[31] = 1346269;
            _fibonacci[32] = 2178309;
            _fibonacci[33] = 3524578;
        }

        public int GetFibonacciOrdinal(int num)
        {
            
            return _fibonacci.Where(x => x.Value == num).FirstOrDefault().Key;

        }
    }
}
