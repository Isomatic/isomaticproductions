﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Numbers.Models;
using Numbers.Models.Contracts;

namespace Numbers.DAL
{
    public class NumberRepository : INumberRepository
    {
        private List<Number> _numbers = new List<Number>();

        public void Add(Number number)
        {
            _numbers.Add(number);
        }

        public Number Get(int number)
        {
            return _numbers.FirstOrDefault(i => i.Value == number);
        }

        public List<Number> GetAll()
        {
            return _numbers;
        }
    }
}
