﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Numbers.Models;

namespace Numbers.DAL
{
    public class GematriaStatsRepository
    {
        private readonly List<Gematria> _gematria;

        public GematriaStatsRepository()
        {
            _gematria = new List<Gematria>();
        }

        public List<Gematria> GetGematria()
        {
            return _gematria;
        }

        public Gematria AddGematria(Gematria gematria)
        {
            _gematria.Add(gematria);
            return gematria;
        }
    }
}
