﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Numbers.BLL;
using Numbers.BLL.Contracts;
using Numbers.Models;

namespace WordDecoder
{
    class GematriaDecoder
    {
        static void Main(string[] args)
        {
            Engine engine = new Engine();
            string persisted = "";

            while (true)
            {
                persisted = engine.Run(persisted);
            }
        }
    }
}
