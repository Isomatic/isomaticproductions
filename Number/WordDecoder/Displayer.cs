﻿using System;
using System.Linq;
using Numbers.Models;
using Numbers.BLL;

namespace WordDecoder
{
    public class Displayer
    {
        public void DisplayGematria(string word)
        {
            GematriaCalculator calc = new GematriaCalculator();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("\"{0}\" in the Jewish system equals {1}", word, calc.Gematrinate(word, GematriaTypes.Jewish));
            Console.ResetColor();        
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("\"{0}\" in the English Ordinal system equals {1}", word, calc.Gematrinate(word, GematriaTypes.EnglishOrdinal));           
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("\"{0}\" in the Reverse Ordinal system equals {1}", word, calc.Gematrinate(word, GematriaTypes.ReverseOrdinal));
            Console.BackgroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("\"{0}\" in the Full Reduction system equals {1}", word, calc.EnglishReduction(word));
            Console.ResetColor();
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("\"{0}\" in the Reverse Reduced system equals {1}", word, calc.Gematrinate(word, GematriaTypes.ReverseReduced));
            Console.ResetColor();

            if (Bacon(word))
            {
                Console.ForegroundColor = ConsoleColor.DarkMagenta;
                Console.WriteLine("\"{0}\" in the Francis Bacon system equals {1}", word, calc.Gematrinate(word, GematriaTypes.EnglishOrdinal) + Baconate(word));
                Console.ResetColor();
            }

            if (word.ToLower().Contains('s'))
            {
                Console.ForegroundColor = ConsoleColor.DarkCyan;
                Console.WriteLine("\"{0}\" in the Full Reduction system (S) equals {1}", word, calc.SException(word));
                Console.ResetColor();
            }
            if (word.ToLower().Contains('k') || word.ToLower().Contains('v'))
            {
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("\"{0}\" in the Full Reduction system (K/V) equals {1}", word, calc.Gematrinate(word, GematriaTypes.PythagoreanKv));
                Console.ResetColor();
            }
            if (word.ToLower().Contains('s') && (word.ToLower().Contains('k') || word.ToLower().Contains('v')))
            {
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine("\"{0}\" in the Full Reduction system (K/V+S) equals {1}", word, calc.KvsException(word));
                Console.ResetColor();
            }
        }

        public void DisplayAltGematria(string word)
        {
            GematriaCalculator calc = new GematriaCalculator();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("\"{0}\" in the ALW Cipher equals {1}", word, calc.Gematrinate(word, GematriaTypes.ALW));
            Console.ResetColor();
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("\"{0}\" in the ALW Cipher Single Reduction equals {1}", word, calc.Gematrinate(word, GematriaTypes.ALW, true));
            Console.ResetColor();
        }

        public void DisplayNumberInfo(int num)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("The sum of the numbers 1 through {0} is {1}", num, Sequence.Sum1ThruNum(num));
            Console.ForegroundColor = ConsoleColor.Red;
            Factorized factorizeResults = Factorizer.Prime(num);
            Console.ForegroundColor = ConsoleColor.White;
            var prime = factorizeResults.Prime;
            if (prime > 0)
            {
                Console.Write("{0} is the {1}{2} prime number", prime, num, GetOrdinal(num));
            }
            Console.ResetColor();

            if (factorizeResults.IsPrime)
            {
                Console.WriteLine(" and {0} is the {1}{2} prime number", num, factorizeResults.PrimePosition, GetOrdinal(factorizeResults.PrimePosition));
            }
            else if (factorizeResults.Prime == -1)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("{0} is a prime number.", num);
                Console.ResetColor();
            }
            else if (prime > 0)
            {
                Console.WriteLine();
            }

            Console.ResetColor();
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine("The sum of {0}'s divisors is {1}", num, factorizeResults.SumOfDivisors);
            Console.ForegroundColor = ConsoleColor.Red;
            int fib = Sequence.Fibonacci(num);

            if (fib > 0)
            {
                Console.Write("{0} is the {1}{2} Fibonacci number", fib, num, GetOrdinal(num));
            }

            var fibOrdinal = Sequence.GetFibonacciOrdinal(num);

            if (fibOrdinal > 1)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine(" and {0} is the {1}{2} Fibonacci number", num, fibOrdinal, GetOrdinal(fibOrdinal));
            }
            else if (fib > 0)
            {
                Console.WriteLine();
            }

            Console.ResetColor();
        }

        public void DisplayDateNumerology(DateTime date)
        {
            DateCalculator calc = new DateCalculator();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("{0:dddd, MMMM d, yyyy} is a date with", date);
            Console.ResetColor();
            Console.ForegroundColor = ConsoleColor.Yellow;
            var lifeLesson = calc.LifeLesson(date);
            var reducedLL = calc.Reduce(lifeLesson);
            Console.Write("a Life Path number of {0}", lifeLesson);
            if (lifeLesson%11 != 0)
                Console.Write(" => {0}", reducedLL);
            if (reducedLL > 9 && lifeLesson % 11 != 0)
                Console.Write(" => {0}", calc.Reduce(reducedLL));
            Console.ResetColor();
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("MM/DD numerology of {0}", calc.MonthDay(date));
            Console.ResetColor();
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("MM/DD/(YY + YY) numerology of {0}", calc.Split(date));
            Console.ResetColor();
            if (calc.Split(date) != calc.PythSplit(date))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("M+M+D+D+(YY+YY) numerology of {0}", calc.PythSplit(date));
            }           
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine("MM/DD/YY numerology of {0}", calc.AbrvSplit(date));
            Console.ResetColor();
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("M+M+D+D+Y+Y+Y+Y numerology of {0}", calc.Pythagorean(date));
            Console.ResetColor();
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine("and is the {0}{1} day of the year, with {2} days remaining.", date.DayOfYear, GetOrdinal(date.DayOfYear), DaysRemaining(date));
            Console.ResetColor();
        }

        public void DisplayDateSpan(DateRange span)
        {
            var calc = new DateCalculator();
            var duration = calc.DateSpan(span.StartDate, span.EndDate).Duration();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("From {0:dddd, MMMM d, yyyy} to {1:dddd, MMMM d, yyyy} is {2} days.", span.StartDate.Date, span.EndDate.Date, duration.TotalDays);
            Console.ResetColor();
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Counting the end date it's a span of {0} days.",
                duration.Add(TimeSpan.FromDays(1)).TotalDays);
            Console.ResetColor();
            Console.WriteLine("Alternative time units");
            Console.WriteLine("{0} months, {1} days", duration.Days / 30, duration.Days % 365 - 1);
            Console.WriteLine("{0} seconds", duration.TotalSeconds);
            Console.WriteLine("{0} minutes", duration.TotalMinutes);
            Console.WriteLine("{0} hours", duration.TotalHours);
            Console.Write("{0} weeks ", duration.Days / 7);
            Console.WriteLine("and {0} days", duration.Days % 7);
            Console.WriteLine("{0}% of a common year (365 days)", duration.TotalDays / 365 * 100);
        }

        private string GetOrdinal(int? num)
        {
            string number = num.ToString();

            if (number.EndsWith("11") || number.EndsWith("12") || number.EndsWith("13"))
            {
                return "th";
            }
            else if (num % 10 == 1)
            {
                return "st";
            }
            else if (num % 10 == 2)
            {
                return "nd";
            }
            else if (num % 10 == 3)
            {
                return "rd";
            }
            else
            {
                return "th";
            }
        }

        private int Baconate(string word)
        {
            int capitals = 0;

            foreach (var letter in word)
            {
                if (char.IsUpper(letter))
                {
                    capitals += 26;
                }
            }

            return capitals;
        }

        private bool Bacon(string word)
        {
            foreach (var letter in word)
            {
                if (char.IsUpper(letter))
                {
                    return true;
                }
            }

            return false;
        }

        private int DaysRemaining(DateTime date)
        {
            var common = 365 - date.DayOfYear;
            var leap = 366 - date.DayOfYear;

            if (date.Year % 4 != 0)
                return common;
            else if (date.Year % 100 != 0)
                return leap;
            else if (date.Year % 400 != 0)
                return common;
            else return leap;
        }
    }
}
