﻿using Numbers.BLL;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace WordDecoder
{
    public class Engine
    {
        public string Run(string persisted)
        {
            Displayer results = new Displayer();

            string word = Console.ReadLine();
            int num;
            DateTime date;
            var dateSpan = word == null ? new DateRange() : DateSpan(word);

            if (word == null)
            {
                Console.Clear();
            }
            else if (string.IsNullOrWhiteSpace(word))
            {
                ;
            }
            //CTRL+P - Search default browser for propaganda.
            else if (word.Contains("\u0010"))
            {          
                    word = word.Replace("\u0010", "");
                    Process.Start(string.Format("http://duckduckgo.com?q={0}&iax=1&ia=news", word));                            
            }
            else if (word.Contains("\t"))
            {
                word = word.Replace("\t", "");
                Process.Start(string.Format("https://duckduckgo.com/?q={0}&iax=1&ia=images", word));
            }
            //CTRL+G - Search Gematrinator.com
            else if (word.Contains("\a"))
            {
                word = word.Replace("\a", "");
                Process.Start(string.Format("http://www.gematrinator.com/calculator/index.php?Phrase={0}&Ciphers=0-4-8-9", word));
            }
            //CTRL+P - Prompt date numerology for Predictions
            else if (word == "")
            {
                int n = 0;
                string input = "-";
                DateCalculator calc = new DateCalculator();
                IEnumerable<DateTime> predictedDates = calc.LoopThrough();

                while (!string.IsNullOrWhiteSpace(input))
                {
                    Console.WriteLine("Enter a number you want a date for.");
                    input = Console.ReadLine();
                    if (int.TryParse(input, out n))
                        predictedDates = predictedDates.Where(x => calc.LifeLesson(x) == n || calc.Pythagorean(x) == n
                            || calc.AbrvSplit(x) == n || calc.Split(x) == n || calc.MonthDay(x) == n);
                }

                foreach (var d in predictedDates)
                {
                    Console.WriteLine(d);
                }
            }
            else if (dateSpan.HasDates)
            {
                results.DisplayDateSpan(dateSpan);
            }
            //CTRL+T - Get Today's date
            else if (DateTime.TryParse(word, out date) || word == "")
            {
                if (word == "")
                    date = DateTime.Now;
                results.DisplayDateNumerology(date);
            }
            else if (int.TryParse(word, out num) && !word.StartsWith("-"))
            {
                results.DisplayNumberInfo(num);
            }
            else if (word.Contains("+"))
            {
                int temp = 0;
                temp = Arithmetic.Add(word);
                Console.WriteLine(temp);
            }
            else if (word.StartsWith("-"))
            {
                int temp = 0;
                temp = Arithmetic.Subtract(word, temp);
                Console.WriteLine(temp);
            }
            else
            {
                results.DisplayGematria(word);
                results.DisplayAltGematria(word);
            }

            Console.WriteLine();

            return persisted;
        }

        private DateRange DateSpan(string word)
        {
            string[] tokens = { "-", "to", " " };
            string[] dateStrings = { "" };
            var dates = new List<DateTime>();

            for (int i = 0; i < tokens.Length; i++)
            {
                dateStrings = word.Split(tokens[i].ToCharArray());
            }

            for (int i = 0; i < dateStrings.Length; i++)
            {
                DateTime date;

                if (DateTime.TryParse(dateStrings[i], out date))
                {
                    dates.Add(date);
                }
            }

            if (dates.Count == 2)
                if (dates[0] > dates[1])
                {
                    var temp = new DateTime();
                    temp = dates[0];
                    dates[0] = dates[1];
                    dates[1] = temp;
                }

            var range = dates.Count == 2 ? new DateRange(dates.Count == 2, dates[0], dates[1]) : new DateRange();

            return range;
        }
    }
}
