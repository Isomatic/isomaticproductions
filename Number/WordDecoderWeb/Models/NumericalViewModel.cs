﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WordDecoderWeb.Models
{
    public class NumericalViewModel
    {
        public int Number { get; set; }
        public int CompositeSum { get; set; }
        public int? Prime { get; set; }
        public int? PrimeOrdinal { get; set; }
        public List<int> Divisors { get; set; }
        public int SumOfDivisors { get; set; }
        public int Fibonacci { get; set; }
        public int FibonacciOrdinal { get; set; }

        public NumericalViewModel()
        {
            Divisors = new List<int>();
        }
    }
}