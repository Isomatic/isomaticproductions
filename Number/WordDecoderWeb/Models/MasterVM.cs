﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Numbers.Models;
using Numbers.BLL;


namespace WordDecoderWeb.Models
{
    public class MasterVM
    {
        public Word Word { get; set; }
        public GematriaCalculator Calculator { get; }
    }
}