﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Numbers.Models;

namespace WordDecoderWeb.Models
{
    public class StatsIndexViewModel
    {
        public string Input { get; set; }
        public List<Gematria> GematriaList { get; set; }
        public Dictionary<int, int> Tally { get; set; } 
    }
}