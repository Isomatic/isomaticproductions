﻿using Numbers.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WordDecoderWeb.Models
{
    public class GematriaValueViewModel
    {
        public string Word { get; set; }
        public List<Word> Words { get; set; }
        public NumericalViewModel NumericalModel { get; set; }
        public GematriaValueViewModel()
        {
            Words = new List<Word>();
            NumericalModel = new NumericalViewModel();
        }
    }
}