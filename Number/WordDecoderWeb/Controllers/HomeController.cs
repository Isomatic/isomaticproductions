﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Numbers.Models;
using Numbers.BLL;
using WordDecoderWeb.Models;

namespace WordDecoderWeb.Controllers
{
    public class HomeController : Controller
    {
        private static List<Word> _words;

        public static List<Word> Words
        {
            get
            {
                if (_words == null)
                {
                    _words = new List<Word>();
                }
                return _words;
            }
        }

        [HttpGet]
        public ActionResult Index(GematriaValueViewModel model)
        {
            return View(model);
        }

        [HttpPost]
        public PartialViewResult Calculate(string word)
        {
            int num = 0;

            if (int.TryParse(word, out num))
            {
                var factorInfo = Factorizer.Prime(num);
                var model = new NumericalViewModel
                {
                    Number = num,
                    CompositeSum = Sequence.Sum1ThruNum(num),
                    Prime = factorInfo.Prime,
                    PrimeOrdinal = factorInfo.PrimePosition,
                    Fibonacci = Sequence.Fibonacci(num),
                    FibonacciOrdinal = Sequence.GetFibonacciOrdinal(num),
                    SumOfDivisors = factorInfo.SumOfDivisors,
                    Divisors = factorInfo.Divisors
                };

                model.Divisors.Sort();
                return PartialView("_NumericInfo", model);
            } 

            GematriaCalculator calc = new GematriaCalculator();
            var inputWord = new Word();
            inputWord.Name = word;
           //  ValueAssigner.AssignValues(inputWord);
            Words.Add(inputWord);

            return PartialView("_ResultsTable", Words);
        }

        [HttpPost]
        public PartialViewResult Clear()
        {
            Words.Clear();
            return PartialView("_ResultsTable", Words);
        }
    }
}