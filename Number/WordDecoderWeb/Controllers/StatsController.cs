﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Mvc;
using Numbers.BLL;
using Numbers.Models;
using WordDecoderWeb.Models;

namespace WordDecoderWeb.Controllers
{
    public class StatsController : Controller
    {
        private static List<Gematria> _gematria;
        private static Dictionary<int, int> _tally;

        public static List<Gematria> Gematria
        {
            get
            {        
                    _gematria = _gematria ?? new List<Gematria>();
                return _gematria;
            }
        }

        public static Dictionary<int, int> Tally
        {
            get
            {         
                    _tally = _tally ?? new Dictionary<int, int>();
                return _tally;
            }
        } 

        private readonly StatisticsService _statisticsService;

        public StatsController()
        {
            _statisticsService = new StatisticsService();
        }
        // GET: Stats
        [HttpGet]
        public ActionResult Index()
        {
            var model = new StatsIndexViewModel
            {
                GematriaList = Gematria,
                Tally = Tally
            };
            
            return View(model);
        }

        public PartialViewResult Calculate(StatsIndexViewModel model)
        {
            _gematria = _statisticsService.Compile(model.Input, _gematria, _tally);
  
            model.GematriaList = _gematria;
            model.Tally = _tally;

            return PartialView("_Chart", model);
        }

        public PartialViewResult Reset()
        {
            _gematria.Clear();
            _tally.Clear();

            var model = new StatsIndexViewModel
            {
                GematriaList = _gematria,
                Tally = _tally
            };

            return PartialView("_Chart", model);
        }
    }
}