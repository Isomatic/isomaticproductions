﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numeria
{
    public class Caster
    {
        public string Name { get; set; }
        public string Class { get; set; }
        public int Health { get; set; }
        public int Np { get; set; }
        public int Attack { get; set; }
        public int Defense { get; set; }
        public int Luck { get; set; }
        public int Special { get; set; }
    }
}
