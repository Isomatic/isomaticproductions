﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Numbers.BLL;

namespace Numeria
{
    class Engine
    {
        public void Play()
        {
            var player = new Caster
            {
                Name = "Chilvan",
                Class = "Convoluter",
                Attack = 30,
                Defense = 50,
                Health = 100,
                Luck = 9,
                Np = 100,
                Special = 0
            };

            var opponent = new Caster
            {
                Name = "Gandimare",
                Class = "Galvaniere",
                Attack = 75,
                Defense = 90,
                Health = 300,
                Luck = 2,
                Np = 1000,
                Special = 0
            };

            Random rng = new Random();
            
            var prime = Factorizer.Prime(rng.Next(2000)).IsPrime;
            if (prime)
            {
                Console.WriteLine("Attack!!");
            }
        }
    }
}
